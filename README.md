# brain-dap

Deep Audio Processing(DAP)

- speech diarization(diarize)
  - wav to text
- speaker verification(ghostvlad, dvectorize)
  - mel(or wav) to vector
- speech separation(vfilter)
  - (wav, vector) to wav
- speech enhancement(cnnoise)
  - wav to wav
