cmake_minimum_required(VERSION 2.8.12)

install(DIRECTORY ./maum/brain/dap
    DESTINATION include/maum/brain
    FILES_MATCHING PATTERN "*.proto")
