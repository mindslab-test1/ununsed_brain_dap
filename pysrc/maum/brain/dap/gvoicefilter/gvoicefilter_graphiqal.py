import os
import sys
import torch
import numpy as np

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc/maum/brain/dap/gvoicefilter')
from model.model import VoiceFilter
from utils.utils import read_wav
from utils.hparams import load_hparam_str
from utils.audio import Audio


class GVoiceFilterServicer:
    def __init__(self, checkpoint_path, device):
        chkpt = torch.load(checkpoint_path, map_location='cpu')
        hp = load_hparam_str(chkpt['hp_str'])
        self.hp = hp

        os.environ["CUDA_VISIBLE_DEVICES"] = str(device)
        self.device = device
        self.model = VoiceFilter(hp).cuda()
        self.model.load_state_dict(chkpt['model'])
        self.model.eval()

        self.audio = Audio(hp)

        del chkpt
        torch.cuda.empty_cache()

    def GetFilteredFromVecWav(self, gvlad, wav):
        with torch.no_grad():
            gvlad = torch.tensor(gvlad).cuda().float()
            wav = torch.tensor(wav).cuda().float()
            gvlad = gvlad.unsqueeze(0)
            wav = wav.unsqueeze(0)
            mag, phase = self.model.get_mag_phase(wav)

            est_mask = self.model(mag, gvlad)
            est_mag = mag * est_mask
            est_mag = self.model.post_spec(est_mag)

            est_mag = est_mag[0].cpu().detach().numpy()
            phase = phase[0].cpu().detach().numpy()
            est_wav = self.audio.spec2wav(est_mag, phase)

            return est_wav.tolist()


def compare_list(x, y):
    return np.max(np.abs(np.array(x) - np.array(y))) < 1e-6


if __name__ == '__main__':
    sr, ref_wav_input1 = read_wav(
        os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'gvoicefilter', 'simahn-cut-16k.wav'))
    ref_gvlad_input1 = torch.load(
        os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'gvoicefilter', 'sim-sample.gvlad'), map_location='cpu').tolist()
    ref_gvlad_input2 = torch.load(
        os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'gvoicefilter', 'ahn-sample.gvlad'), map_location='cpu').tolist()

    sr, ref_wav_output1 = read_wav(
        os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'gvoicefilter', 'sim-result.wav'))
    sr, ref_wav_output2 = read_wav(
        os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'gvoicefilter', 'ahn-result.wav'))

    model = GVoiceFilterServicer(
        checkpoint_path=os.path.join(os.environ['MAUM_ROOT'], 'trained', 'dap', 'gvoicefilter', 'minlevel60-chkpt_300.pt'),
        device=0)

    result_output1 = model.GetFilteredFromVecWav(ref_gvlad_input1, ref_wav_input1)
    result_output2 = model.GetFilteredFromVecWav(ref_gvlad_input2, ref_wav_input1)  # 'ref_wav_input1' is not a typo.

    assert compare_list(result_output1, ref_wav_output1)
    assert compare_list(result_output2, ref_wav_output2)
