import torch
import torch.nn as nn
from mir_eval.separation import bss_eval_sources


def validate(audio, model, testloader, writer, step):
    model.eval()
    
    criterion = nn.MSELoss()
    with torch.no_grad():
        for gvlad, target_wav, mixed_wav in testloader:

            target_mag, _ = model.get_mag_phase(target_wav.cuda())
            mixed_mag, mixed_phase = model.get_mag_phase(mixed_wav.cuda())

            est_mask = model(mixed_mag, gvlad.cuda())
            est_mag = est_mask * mixed_mag
            test_loss = criterion(target_mag, est_mag).item()

            est_mag_temp = model.post_spec(est_mag)

            mixed_mag = mixed_mag[0].cpu().detach().numpy()
            target_mag = target_mag[0].cpu().detach().numpy()
            est_mag = est_mag[0].cpu().detach().numpy()
            est_mag_temp = est_mag_temp[0].cpu().detach().numpy()
            mixed_phase = mixed_phase[0].cpu().detach().numpy()

            est_wav = audio.spec2wav(est_mag_temp, mixed_phase)
            est_mask = est_mask[0].cpu().detach().numpy()

            sdr = bss_eval_sources(target_wav.numpy(), est_wav, False)[0][0]
            writer.log_evaluation(test_loss, sdr,
                                  mixed_wav[0], target_wav[0], est_wav,
                                  mixed_mag, target_mag, est_mag, est_mask,
                                  step)
            break

    model.train()
