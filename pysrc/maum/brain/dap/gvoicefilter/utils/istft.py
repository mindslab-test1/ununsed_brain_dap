import scipy
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import librosa


def create_istft_fn(hp):
    return ISTFT(
        n_fft=(hp.num_freq - 1) * 2,
        hop_length=hp.hop_length,
        win_length=hp.win_length,
    ).cuda()


class ISTFT(nn.Module):
    def __init__(self, n_fft, hop_length, win_length, window='hann'):
        super(ISTFT, self).__init__()
        self.n_fft = n_fft
        self.num_freq = 1 + (self.n_fft // 2)
        self.hop_length = hop_length
        self.win_length = win_length
        self.window = window

        self.ifft_window = scipy.signal.get_window(self.window, self.win_length, fftbins=True)
        self.ifft_window = librosa.util.pad_center(self.ifft_window, self.n_fft)
        self.ifft_window = self.ifft_window.astype(np.float32)
        self.ifft_window = torch.from_numpy(self.ifft_window).cuda() # [n_fft]

        self.conj = torch.tensor([1., -1.], requires_grad=False, dtype=torch.float32).cuda()
        self.eye = torch.eye(self.n_fft, requires_grad=False, dtype=torch.float32).cuda()
        self.eye = self.eye.unsqueeze(1) # [n_fft, 1, n_fft]

    def forward(self, x):
        # input dimension: [num_freq, T, 2]
        # x corresponds to `stft_matrix` of librosa/core/spectrum.py

        n_frames = x.shape[1]
        expected_signal_len = self.n_fft + self.hop_length * (n_frames - 1)

        x = torch.cat((x, self.conj * x.flip(dims=(0,))[1:-1]), dim=0) # [a,b,c,d,e] -> [a,b,c,d,e,d,c,b]
        # n_fft = 2*(num_freq-1)
        # [n_fft, T, 2]

        x = x.transpose(0, 1)
        x = torch.ifft(x, signal_ndim=1)[:, :, 0] # get real part of ifft
        x = x.transpose(0, 1)

        # [n_fft, T]
        x = x.transpose(0, 1)
        x = x*self.ifft_window # self.ifft_window: [n_fft]
        x = x.transpose(0, 1)
        x = x.unsqueeze(0)
        # [1, n_fft, T]
        # this is stack of `ytmp` in librosa/core/spectrum.py

        x = F.conv_transpose1d(x, self.eye, stride=self.hop_length, padding=0)
        x = x.view(-1)
        assert x.size(0) == expected_signal_len

        window_sum = window_sumsquare(self.window, n_frames, hop_length=self.hop_length,
            win_length=self.win_length, n_fft=self.n_fft, dtype=np.float32)

        approx_nonzero_indices = torch.from_numpy(
            np.where(window_sum > librosa.util.tiny(window_sum))[0]).cuda().requires_grad_(False)
        window_sum = torch.from_numpy(window_sum).cuda().requires_grad_(False)
        x[approx_nonzero_indices] /= window_sum[approx_nonzero_indices]

        x = x[self.n_fft//2:-self.n_fft//2]

        return x


# from librosa 0.6
def window_sumsquare(window, n_frames, hop_length=512, win_length=None, n_fft=2048,
                     dtype=np.float32, norm=None):
    if win_length is None:
        win_length = n_fft

    n = n_fft + hop_length * (n_frames - 1)
    x = np.zeros(n, dtype=dtype)

    # Compute the squared window at the desired length
    win_sq = scipy.signal.get_window(window, win_length, fftbins=True)
    win_sq = librosa.util.normalize(win_sq, norm=norm)**2
    win_sq = librosa.util.pad_center(win_sq, n_fft)

    # Fill the envelope
    n = len(x)
    n_fft = len(win_sq)
    for i in range(n_frames):
        sample = i * hop_length
        x[sample:min(n, sample + n_fft)] += win_sq[:max(0, min(n_fft, n - sample))]

    return x
