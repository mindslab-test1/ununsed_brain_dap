import os
import glob
import torch
import argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read

from maum.brain.dap.ghostvlad.core.hparams import HParam
from maum.brain.dap.ghostvlad.core.utils import cosine_similarity, wav_formatter
from maum.brain.dap.ghostvlad.core.model import VGGSR


def main(hp, args):
    model = VGGSR(hp).cuda()
    checkpoint = torch.load(args.checkpoint_path)
    model.load_state_dict(checkpoint['model'])
    model.eval()

    embeddings = list()

    with torch.no_grad():
        for wavpath in glob.glob(os.path.join(args.input_path, '*.wav')):
            print(wavpath)
            sr, wav = read(wavpath)
            assert sr == hp.audio.sr

            wav = wav_formatter(wav)
            wav = torch.from_numpy(wav).cuda()
            wav = wav.unsqueeze(0)
            result = model.inference(wav)[0]
            embeddings.append(result.cpu())

    embeddings = torch.stack(embeddings).cuda()
    similarity = cosine_similarity(embeddings, embeddings)
    similarity = similarity.cpu().numpy()

    fig, ax = plt.subplots(figsize=(6, 4))
    im = ax.imshow(similarity, aspect='auto', origin='lower',
                   interpolation='none')
    fig.colorbar(im, ax=ax)
    xlabel = 'Speaker'
    plt.title(args.checkpoint_path)
    plt.xlabel(xlabel)
    plt.ylabel('Speaker')
    plt.tight_layout()
    plt.show()

if __name__ == '__main__':	
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="yaml file for config.")
    parser.add_argument('-p', '--checkpoint_path', required=True,
                        help="path of checkpoint pt file for evaluation")
    parser.add_argument('-i', '--input_path', type=str, default=None,
    					help="path of utterances to compare")
    args = parser.parse_args()
    hp = HParam(args.config)

    main(hp, args)
