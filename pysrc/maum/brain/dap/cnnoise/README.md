# cnnoise

- STFT한 뒤 spectrogram masking 하는 것으로 변경!
- L2 loss with raw audio: 실패 
    - https://arxiv.org/abs/1806.10522 using only L2 loss
    - https://github.com/francoisgermain/SpeechDenoisingWithDeepFeatureLosses

metrics: 

- 일단은 SSNR 씀

- "EVALUATION AND VALIDATION OF A NEW PERCEPTUAL SPEECH
  QUALITY MEASURE FOR SPEECH ENHANCEMENT" (LLR, SSNR, WSS, PESQ 로 CSIG, CBAK 계산하는 방법)
- `\K14513_CD_Files\MATLAB_code\objective_measures\quality` @ https://www.crcpress.com/downloads/K14513/K14513_CD_Files.zip (MATLAB. LLR, SSNR, WSS, PESQ 계산 가능)

