import os
import sys
import torch
import numpy as np

sys.path.insert(0, '/home/msl/swpark/brain-dap/pysrc/maum/brain/dap/cnnoise')
from model.model import CNNoise
from utils.utils import read_wav
from utils.hparams import load_hparam_str
from utils.audio import Audio


class CNNoiseServicer:
    def __init__(self, checkpoint_path, device):
        chkpt = torch.load(checkpoint_path, map_location='cpu')
        hp = load_hparam_str(chkpt['hp_str'])
        self.hp = hp

        #torch.cuda.set_device(device)
        os.environ["CUDA_VISIBLE_DEVICES"] = str(device)
        self.device = device
        self.model = CNNoise(hp).cuda()
        self.model.load_state_dict(chkpt['model'])
        self.model.eval()

        self.audio = Audio(hp)

        del chkpt
        torch.cuda.empty_cache()

    def GetDenoiseFromWavPart(self, wav):
        with torch.no_grad():
            wav = torch.tensor(wav)
            wav = wav.cuda().float()
            wav = wav.unsqueeze(0)
            mag, phase = self.model.get_mag_phase(wav)

            est_mask = self.model(mag)
            est_mag = mag * est_mask
            est_mag = self.model.post_spec(est_mag)

            est_mag = est_mag[0].cpu().detach().numpy()
            phase = phase[0].cpu().detach().numpy()
            est_wav = self.audio.spec2wav(est_mag, phase)

            return est_wav # np.ndarray

    def GetDenoiseFromWav(self, wav):
        wavlen = len(wav)
        max_chunk_size = self.hp.audio.sr * 14 # max 14 seconds
        result = list()

        for idx in range((wavlen+(max_chunk_size-1)) // max_chunk_size):
            wav_part = wav[idx*max_chunk_size:(idx+1)*max_chunk_size]
            result_wav = self.GetDenoiseFromWavPart(wav_part)
            result.append(result_wav)

        result = np.concatenate(result)
        result = result.tolist()
        return result

def compare_list(x, y):
    assert np.max(np.abs(np.array(x) - np.array(y))) < 1e-6


if __name__ == '__main__':
    sr, ref_input1 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'noisy.wav'))
    sr, ref_input2 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'curry-16k-cut.wav'))
    sr, ref_input3 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'moon2-cut.wav'))
    sr, ref_input4 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'msl-seo-16k-cut.wav'))
    sr, ref_input5 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'in1.wav'))

    sr, ref_output1 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'noisy-denoised.wav'))
    sr, ref_output2 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'curry-16k-cut-denoised.wav'))
    sr, ref_output3 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'moon2-cut-denoised.wav'))
    sr, ref_output4 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'msl-seo-16k-cut-denoised.wav'))
    sr, ref_output5 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'cnnoise', 'in1-denoised.wav'))

    model = CNNoiseServicer(
        checkpoint_path=os.path.join(os.environ['MAUM_ROOT'], 'trained', 'dap', 'cnnoise', 'fixdata-snr5_11170c5_220.pt'),
        device=0)

    result_output1 = model.GetDenoiseFromWav(ref_input1)
    result_output2 = model.GetDenoiseFromWav(ref_input2)
    result_output3 = model.GetDenoiseFromWav(ref_input3)
    result_output4 = model.GetDenoiseFromWav(ref_input4)
    result_output5 = model.GetDenoiseFromWav(ref_input5)

    compare_list(ref_output1, result_output1)
    compare_list(ref_output2, result_output2)
    compare_list(ref_output3, result_output3)
    compare_list(ref_output4, result_output4)
    compare_list(ref_output5, result_output5)
