import torch
import torch.nn as nn
import torch.nn.functional as F


class CNNoise(nn.Module):
    def __init__(self, hp):
        super(CNNoise, self).__init__()
        self.hp = hp
        assert hp.audio.n_fft // 2 + 1 == hp.audio.num_freq == hp.model.fc2_dim, \
            "stft-related dimension mismatch"

        self.conv = nn.Sequential(
            # cnn1
            nn.ZeroPad2d((0, 0, 3, 3)),
            nn.Conv2d(1, 64, kernel_size=(7, 1), dilation=(1, 1)),
            nn.BatchNorm2d(64), nn.LeakyReLU(),

            # cnn2
            nn.ZeroPad2d((3, 3, 0, 0)),
            nn.Conv2d(64, 64, kernel_size=(1, 7), dilation=(1, 1)),
            nn.BatchNorm2d(64), nn.LeakyReLU(),

            # cnn3
            nn.ZeroPad2d(2),
            nn.Conv2d(64, 64, kernel_size=(5, 5), dilation=(1, 1)),
            nn.BatchNorm2d(64), nn.LeakyReLU(),

            # cnn4
            nn.ZeroPad2d((4, 4, 2, 2)),
            nn.Conv2d(64, 64, kernel_size=(5, 5), dilation=(1, 2)), # (5, 9)
            nn.BatchNorm2d(64), nn.LeakyReLU(),

            # cnn5
            nn.ZeroPad2d((8, 8, 2, 2)),
            nn.Conv2d(64, 64, kernel_size=(5, 5), dilation=(1, 4)), # (5, 17)
            nn.BatchNorm2d(64), nn.LeakyReLU(),

            # cnn6
            nn.ZeroPad2d((16, 16, 2, 2)),
            nn.Conv2d(64, 64, kernel_size=(5, 5), dilation=(1, 8)), # (5, 33)
            nn.BatchNorm2d(64), nn.LeakyReLU(),

            # cnn7
            nn.ZeroPad2d((32, 32, 2, 2)),
            nn.Conv2d(64, 64, kernel_size=(5, 5), dilation=(1, 16)), # (5, 65)
            nn.BatchNorm2d(64), nn.LeakyReLU(),

            # cnn8
            nn.Conv2d(64, 8, kernel_size=(1, 1), dilation=(1, 1)), 
            nn.BatchNorm2d(8), nn.LeakyReLU(),
        )
        self.lstm = nn.LSTM(
            8*hp.audio.num_freq,
            hp.model.lstm_dim,
            batch_first=True,
            bidirectional=True)
        self.fc1 = nn.Linear(2*hp.model.lstm_dim, hp.model.fc1_dim)
        self.fc2 = nn.Linear(hp.model.fc1_dim, hp.model.fc2_dim)

        self.register_buffer('window', torch.hann_window(window_length=hp.audio.win_length))

    def get_mag_phase(self, x):
        x = torch.stft(x, n_fft=self.hp.audio.n_fft,
                       hop_length=self.hp.audio.hop_length,
                       win_length=self.hp.audio.win_length,
                       window=self.window) # [B, num_freq, T, 2]
        mag = torch.norm(x, p=2, dim=-1) # [B, num_freq, T]
        mag = self.pre_spec(mag)
        phase = torch.atan2(x[:, :, :, 1], x[:, :, :, 0]) # [B, num_freq, T]
        return mag, phase

    def pre_spec(self, x):
        return self.normalize(self.amp_to_db(x) - self.hp.audio.ref_level_db)

    def post_spec(self, x):
        return self.db_to_amp(self.denormalize(x) + self.hp.audio.ref_level_db)

    def amp_to_db(self, x):
        return 20.0 * torch.log10(torch.max(torch.tensor([1e-5]).cuda(), x))

    def db_to_amp(self, x):
        return torch.pow(torch.tensor([10.0]).cuda(), 0.05*x)

    def normalize(self, x):
        return torch.clamp(x / -self.hp.audio.min_level_db, -1.0, 0.0) + 1.0

    def denormalize(self, x):
        return (torch.clamp(x, 0.0, 1.0) - 1.0) * -self.hp.audio.min_level_db

    def forward(self, x):
        x = x.unsqueeze(1) # [B, 1, num_freq, T]
        x = self.conv(x) # [B, 8, num_freq, T]
        x = x.flatten(start_dim=1, end_dim=2) # [B, 8*num_freq, T]
        x = x.transpose(1, 2).contiguous() # [B, T, 8*num_freq]

        x, _ = self.lstm(x) # [B, T, 2*lstm_dim]
        x = F.leaky_relu(x)
        x = self.fc1(x) # [B, T, fc1_dim]
        x = F.leaky_relu(x)
        x = self.fc2(x) # [B, T, fc2_dim]
        x = torch.sigmoid(x)
        x = x.transpose(1, 2) # [B, fc2_dim, T]
        return x
