import torch
import torch.nn as nn
import numpy as np

from .utils import get_ssnr


def validate(audio, model, testloader, writer, step):
    model.eval()
    criterion = nn.MSELoss()
    for clean, noisy in testloader:
        clean_mag, _ = model.get_mag_phase(clean.cuda())
        noisy_mag, noisy_phase = model.get_mag_phase(noisy.cuda())

        est_mask = model(noisy_mag)
        est_mag = noisy_mag * est_mask
        test_loss = criterion(est_mag, clean_mag).item()

        est_mag_temp = model.post_spec(est_mag)

        noisy_mag = noisy_mag[0].cpu().detach().numpy()
        clean_mag = clean_mag[0].cpu().detach().numpy()
        est_mag = est_mag[0].cpu().detach().numpy()
        est_mag_temp = est_mag_temp[0].cpu().detach().numpy()
        noisy_phase = noisy_phase[0].cpu().detach().numpy()
        est_mask = est_mask[0].cpu().detach().numpy()

        est_wav = audio.spec2wav(est_mag_temp, noisy_phase)
        clean = clean[0].numpy()
        noisy = noisy[0].numpy()
        ssnr = get_ssnr(clean, est_wav)

        writer.log_evaluation(test_loss, ssnr,
            clean, est_wav, noisy,
            noisy_mag, clean_mag, est_mag, est_mask,
            step)
        break
    
    model.train()
