import grpc
import google.protobuf.empty_pb2 as empty

from ...dap_pb2_grpc import DvectorizeStub
from maum.brain.dap.dap_pb2 import WavBinary


class DvectorizeClient(object):
    def __init__(self, remote='127.0.0.1:41001', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = DvectorizeStub(channel)
        self.chunk_size = chunk_size

    def get_dvec(self, mel):
        return self.stub.GetDvector(mel)

    def get_dvec_from_wav(self, wav_binary):
        if type(wav_binary) == bytes:
            wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.GetDvectorFromWav(wav_binary)

    def get_emb_config(self):
        return self.stub.GetEmbConfig(empty.Empty())

    def get_mel_config(self):
        return self.stub.GetMelConfig(empty.Empty())

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield WavBinary(bin=wav_binary[idx:idx+self.chunk_size])
