import os
import sys
import torch
import numpy as np
from spectralcluster import SpectralClusterer

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc/maum/brain/dap/diarize')
from core.utils import read_wav
from core.hparams import load_hparam_str


# this model doesn't use CUDA
class Diarize:
    def __init__(self, checkpoint_path, device):
        chkpt = torch.load(checkpoint_path, map_location='cpu')
        hp = load_hparam_str(chkpt['hp_str'])
        self.hp = hp

        self.clusterer = SpectralClusterer(
            min_clusters=hp.inf.spectral.min_clusters,
            max_clusters=hp.inf.spectral.max_clusters,
            p_percentile=hp.inf.spectral.p_percentile,
            gaussian_blur_sigma=hp.inf.spectral.gaussian_blur_sigma,
        )

    def GetDiarization(self, emblist):
        emblist = np.array(emblist)
        emblist = emblist.reshape(-1, self.hp.model.emb_dim)
        labels = self.clusterer.predict(emblist)
        return labels.tolist()


if __name__ == '__main__':
    ref_input = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'diarize', "dambo-short.pt"))
    ref_output = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'diarize', "dambo-short.diarize"))

    model = Diarize(
        checkpoint_path=os.path.join(os.environ['MAUM_ROOT'], 'trained', 'dap', 'diarize', 'diarization_v0.pt'),
        device=0)

    result_output = model.GetDiarization(ref_input)

    assert ref_output == result_output
