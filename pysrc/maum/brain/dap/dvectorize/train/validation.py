import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.utils.data import DataLoader


def cosine_similarity(vector_list1, vector_list2):
    vector1_size, dim1 = vector_list1.size()
    vector2_size, dim2 = vector_list2.size()
    assert dim1 == dim2
    sizes = vector1_size, vector2_size, dim1
    similarity = F.cosine_similarity(
        vector_list1.unsqueeze(1).expand(sizes),
        vector_list2.unsqueeze(0).expand(sizes),
        2
    )
    return similarity


def sort_by_index(embedding_list, index_list):
    embeddings = torch.stack(embedding_list).cuda()
    indices = torch.stack(index_list).cuda()
    indices, sorted_index = torch.sort(indices)
    embeddings = torch.index_select(embeddings, dim=0, index=sorted_index)
    return embeddings, indices


def validate(model, valset, writer, step):
    val_loader = DataLoader(dataset=valset, batch_size=1, shuffle=False,
                      num_workers=0, pin_memory=False,
                      sampler=None)
    model.eval()
    with torch.no_grad():
        speaker_embeddings = list()
        speaker_indices = list()

        for mel, speaker_idx in val_loader:
            mel = mel[0].cuda().float()
            speaker_emb = model.embedder.inference(mel)
            speaker_idx = speaker_idx[0]

            speaker_embeddings.append(speaker_emb.cpu())
            speaker_indices.append(speaker_idx)

        speaker_embeddings, speaker_indices = sort_by_index(
            speaker_embeddings, speaker_indices)

        speaker_similarity = cosine_similarity(speaker_embeddings,
                                               speaker_embeddings)
        sizes = speaker_indices.size(0), speaker_indices.size(0)
        target = torch.eq(speaker_indices.unsqueeze(1).expand(sizes),
                          speaker_indices.unsqueeze(0).expand(sizes))

        min_diff_rate = 1.0
        eer_thr = 0.0
        eer = 0.0
        far_list = list()
        frr_list = list()

        rejection_cnt = torch.eq(target, 0).sum().item()
        acceptance_cnt = torch.eq(target, 1).sum().item() - speaker_indices.size(0)

        thresholds = [i/100 for i in range(0, 101)]
        for thr in thresholds:
            prediction = torch.ge(speaker_similarity, thr)
            error = torch.ne(target, prediction)

            far = torch.where(target == 0, error, torch.zeros_like(error)).sum().item() / rejection_cnt
            frr = torch.where(target == 1, error, torch.zeros_like(error)).sum().item() / acceptance_cnt
            far_list.append(far)
            frr_list.append(frr)

            diff_rate = abs(far - frr)
            if diff_rate < min_diff_rate:
                min_diff_rate = diff_rate
                eer_thr = thr
                eer = (far + frr) / 2

        # speaker_labels = [
        #     valset.speakers[speaker_idx]
        #     for speaker_idx in speaker_indices
        # ]
        far_list = np.array(far_list)
        frr_list = np.array(frr_list)

        writer.log_evaluation(
            model, speaker_embeddings,# speaker_labels,
            eer, eer_thr, speaker_similarity, thresholds,
            far_list, frr_list, step)

    model.train()
    return eer, eer_thr
