import os
import glob
import torch
import random
from torch.utils.data import Dataset, DataLoader


def create_trainset(hp, args):
    def _collate_fn(batch):
        frames = random.randint(self._hp.data.frame_min, self._hp.data.frame_max)
        return batch[:,:,:,:frames]
    return DataLoader(dataset=TrainDataset(hp, args), batch_size=hp.data.N, shuffle=True,
                      num_workers=hp.data.num_workers, pin_memory=True, drop_last=True,
                      sampler=None)


def create_valset(hp, args):
    return TestDataset(hp, args)


class TrainDataset(Dataset):
    def __init__(self, hp, args):
        self._hp = hp
        self._args = args
        self.speakers = [x for x in glob.glob(os.path.join(hp.data.train_dir, '*'))
                            if os.path.isdir(x)]
        self.utt_list = list()
        for spk in self.speakers:
            temp = glob.glob(os.path.join(spk, '**', hp.data.pt_format.train), recursive=True)
            self.utt_list.append(temp)
        
    def __len__(self):
        return len(self.speakers)

    def __getitem__(self, idx):
        frames = self._hp.data.frame_max
        utt_list = self.utt_list[idx]
        random.shuffle(utt_list)
        mel_list = list()
        cnt = 0
        pt_idx = 0
        while cnt < self._hp.data.M:
            try:
                mel = torch.load(utt_list[pt_idx])
                length = mel.shape[-1]
                if length >= frames:
                    start = random.randint(0, length-frames)
                    mel_list.append(mel[:, start:start+frames])
                    cnt += 1
            except Exception as e:
                print(e)
            pt_idx += 1
        mel_list = torch.stack(mel_list)
        return mel_list.float()


class TestDataset(Dataset):
    def __init__(self, hp, args):
        self._hp = hp
        self._args = args
        self.speakers = [x for x in glob.glob(os.path.join(hp.data.test_dir, '*'))
                            if os.path.isdir(x)]
        assert hp.data.test_N <= len(self.speakers), \
            'given test_N %d is larger than available %d' % (hp.data.test_N, len(self.speakers))
        self.speakers = self.speakers[:hp.data.test_N]
        self.utt_list = list()
        for spk in self.speakers:
            temp = glob.glob(os.path.join(spk, '**', hp.data.pt_format.test), recursive=True)
            self.utt_list.append(temp)

    def __len__(self):
        return self._hp.data.test_M * self._hp.data.test_N

    def __getitem__(self, idx):
        speaker_id = idx // self._hp.data.test_M
        utt_list = self.utt_list[speaker_id]
        pt_selected = utt_list[idx % self._hp.data.test_M]
        mel = torch.load(pt_selected)
        return mel, speaker_id
