import matplotlib
matplotlib.use("Agg")
import matplotlib.pylab as plt
import numpy as np


def save_figure_to_numpy(fig):
    # save it to a numpy array.
    data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
    data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    data = data.transpose(2, 0, 1)
    return data


def plot_speaker_similarity_to_numpy(speaker_similarity, info=None):
    fig, ax = plt.subplots(figsize=(6, 4))
    im = ax.imshow(speaker_similarity, aspect='auto', origin='lower',
                   interpolation='none')
    fig.colorbar(im, ax=ax)
    xlabel = 'Speaker'
    if info is not None:
        xlabel += '\n\n' + info
    plt.xlabel(xlabel)
    plt.ylabel('Speaker')
    plt.tight_layout()

    fig.canvas.draw()
    data = save_figure_to_numpy(fig)
    plt.close()
    return data


def plot_far_frr_to_numpy(thresholds, false_acceptance_rates,
                          false_rejection_rates):
    fig, ax = plt.subplots(figsize=(4, 4))
    ax.plot(thresholds, false_acceptance_rates, alpha=0.5,
               color='green', linestyle='--', label='FAR')
    ax.plot(thresholds, false_rejection_rates, alpha=0.5,
            color='red', label='FRR')

    plt.legend(loc='upper right')

    plt.xlabel("Threshold")
    plt.xlim(0, 1)
    plt.ylabel("Probability")
    plt.ylim(0, 1)
    plt.tight_layout()

    fig.canvas.draw()
    data = save_figure_to_numpy(fig)
    plt.close()
    return data


def plot_roc_to_numpy(false_acceptance_rates, false_rejection_rates):
    fig, ax = plt.subplots(figsize=(4, 4))
    ax.plot(false_acceptance_rates, 1- false_rejection_rates, color='red')

    plt.xlabel("FAR")
    plt.xlim(0, 1)
    plt.ylabel("1 - FRR")
    plt.ylim(0, 1)
    plt.tight_layout()

    fig.canvas.draw()
    data = save_figure_to_numpy(fig)
    plt.close()
    return data
