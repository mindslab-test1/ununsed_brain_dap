cmake_minimum_required(VERSION 2.8.12)

install(PROGRAMS dvectorize/dvectorize_client.py
    DESTINATION samples/dap
    RENAME dvectorize_client.py
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )

install(PROGRAMS diarize/diarize_client.py
    DESTINATION samples/dap
    RENAME diarize_client.py
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )

install(PROGRAMS ghostvlad/ghostvlad_client.py
    DESTINATION samples/dap
    RENAME ghostvlad_client.py
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )