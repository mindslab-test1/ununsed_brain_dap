import os
import sys

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from maum.brain.dap.diarize.run.dvectorize_client import DvectorizeClient
from maum.brain.dap.dap_pb2 import MelSpectrogram


if __name__ == '__main__':
    client = DvectorizeClient()

    dvec_config = client.get_emb_config()
    mel_config = client.get_mel_config()
    print(dvec_config)
    print(mel_config)

    # d-vector from mel-spectrogram
    mel = [0.1 for x in range(mel_config.n_mel_channels * (10 * mel_config.win_length))]
    mel = MelSpectrogram(data=mel, sample_length=160000)
    dvec_data = client.get_dvec(mel).data

    print(len(dvec_data))

    # dvector from float wav data
    with open('sample.wav', 'rb') as rf:
        dvec_data = client.get_dvec_from_wav(rf.read()).data
    print(len(dvec_data))
