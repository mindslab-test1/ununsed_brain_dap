import os
import sys
import grpc
import google.protobuf.empty_pb2 as empty

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from maum.brain.dap.dap_pb2_grpc import GhostVLADStub
from maum.brain.dap.dap_pb2 import Embedding
from maum.brain.dap.dap_pb2 import WavBinary


class GhostVLADClient(object):
    def __init__(self, remote='127.0.0.1:43001', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = GhostVLADStub(channel)
        self.chunk_size = chunk_size

    def get_dvector_from_wav(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.GetDvectorFromWav(wav_binary)

    def get_emb_config(self):
        return self.stub.GetEmbConfig(empty.Empty())

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield WavBinary(bin=wav_binary[idx:idx+self.chunk_size])


if __name__ == '__main__':
    client = GhostVLADClient()

    emb_config = client.get_emb_config()
    print(emb_config)

    with open('sample.wav', 'rb') as rf:
        dvector_result = client.get_dvector_from_wav(rf.read()).data
    print(dvector_result)
